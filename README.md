#Cardinal Health Coding Task

#Technical Stack:
1. Kotlin (Language)Android X Artifacts
2. MVVM Architecture with Data binding
3. Dagger2 (Dependency Injection)
4. Retrofit and OkHttp (Network)
5. RxJava, RxAndroid
6. Dokka Plugin (Generate Technical Documentation)
7. Android Studio (Development Environment)
8. Added UI Unit Test Cases.


#Technical Specification:
#Architecture: The POC built on MVVM architecture with the help of data binding. It strictly follows the Single Responsibility model and also 
segregated code and extracted business logic from view to provide more test coverage for business logic. Also, it implemented a contract between 
activity and fragment, ViewModel and view to provide seamless communication between components.

#Android Stack Used:
1. AppCompatActivity
2. Fragment
3. ConstraintLayout
4. Expresso



#Functionalities Implemented:

1. Implemented Landing Activity and Landing Fragment for the view.
2. Implemented recycler view for gallery gridImplemented sort by published date, date taken and none.
3. Implemented search bases on tagsAdded different build flavors.
4. Added Kotlin DSL (All build files are in kts).
5. Added semantic versioning for build
6. Uses dokka to generate documentation.
7. Included UI tests.
8. Added code documentation you can run using a command at the terminal(./gradlew Dokka for mac machine).
9. Fixed and lint errors you can get lint report using the command on the terminal (./gradlew lint)
10. Fixed ktlint errors you check ktlint report using the command on the terminal(./gradlew ktlint)
