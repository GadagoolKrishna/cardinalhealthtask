package com.android.cardinalhealthtask.view.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.cardinalhealthtask.data.common.NetworkError
import com.android.cardinalhealthtask.model.GalleryFeedData
import com.android.cardinalhealthtask.model.GalleryFeedItem
import com.android.cardinalhealthtask.network.CallbackWrapper
import com.android.cardinalhealthtask.network.RestService
import com.android.cardinalhealthtask.utils.UtilsHelper
import com.android.cardinalhealthtask.view.base.BaseNetworkViewModel
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.ParseException
import java.util.Collections
import java.util.stream.Collectors
import javax.inject.Inject
import kotlin.collections.ArrayList

class HomeViewModel @Inject constructor(restService: RestService) :
    BaseNetworkViewModel(restService) {
    var galleryData: MutableLiveData<ArrayList<GalleryFeedItem>> =
        MutableLiveData<ArrayList<GalleryFeedItem>>()


    var originalGalleryList: ArrayList<GalleryFeedItem> = ArrayList()
    var mutableGalleryList: ArrayList<GalleryFeedItem> = ArrayList()

    /**
     * Get gallery feed data
     */
    fun fetchGalleryFeedData(tag: String) {
        getRestService().getGalleryFeed(UtilsHelper.getQueryMap(tag))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CallbackWrapper<GalleryFeedData>() {
                override fun onSuccess(t: GalleryFeedData) {
                    originalGalleryList.clear()
                    mutableGalleryList.clear()
                    originalGalleryList.addAll(t.galleryItemList)
                    mutableGalleryList.addAll(t.galleryItemList)

                    galleryData.postValue(t.galleryItemList)
                }

                override fun onFailure(error: NetworkError) {
                    showSnackBarAction(error.errorMessage!!, "Okay", 0, Snackbar.LENGTH_LONG)
                }
            })
    }

    fun getGalleryFeedData(): LiveData<ArrayList<GalleryFeedItem>> {
        return galleryData
    }


    /**
     * Sort gallery by published date
     */
    private fun sortGalleryByPublishedDate(galleryList: ArrayList<GalleryFeedItem>) {
        Collections.sort(galleryList, object : Comparator<GalleryFeedItem> {
            override fun compare(o1: GalleryFeedItem, o2: GalleryFeedItem): Int {
                try {
                    return o1.getFormattedPublishedDate().compareTo(o2.getFormattedPublishedDate())
                } catch (pe: ParseException) {
                }
                return 0
            }
        })
    }

    /**
     * Sort gallery by taken date
     */
    private fun sortGalleryTakenDate(galleryList: ArrayList<GalleryFeedItem>) {
        Collections.sort(galleryList, object : Comparator<GalleryFeedItem> {
            override fun compare(o1: GalleryFeedItem, o2: GalleryFeedItem): Int {
                try {
                    return o1.getFormattedDateTaken().compareTo(o2.getFormattedDateTaken())
                } catch (pe: ParseException) {
                }
                return 0
            }
        })
    }

    fun sortByTakenData() {
        sortGalleryTakenDate(mutableGalleryList)
        galleryData.postValue(mutableGalleryList)
    }

    fun sortByPublishedDate() {
        sortGalleryByPublishedDate(mutableGalleryList)
        galleryData.postValue(mutableGalleryList)
    }

    fun sortByNone() {
        mutableGalleryList.clear()
        mutableGalleryList.addAll(originalGalleryList)
        galleryData.postValue(mutableGalleryList)
    }

    fun sortDataUsingStream() {
        galleryData.postValue(
            mutableGalleryList.stream()
                .sorted(Comparator.comparing(GalleryFeedItem::getFormattedDateTaken))
                .collect(Collectors.toList()) as ArrayList<GalleryFeedItem>?
        )
    }
}