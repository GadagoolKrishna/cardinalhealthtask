package com.android.cardinalhealthtask.view.base

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.cardinalhealthtask.R
import com.android.cardinalhealthtask.app.CHApplication
import com.android.cardinalhealthtask.data.common.NetworkError
import com.android.cardinalhealthtask.data.common.UiHelper
import io.reactivex.subjects.PublishSubject

open class BaseViewModel : ViewModel() {

    var uiLiveData: MutableLiveData<UiHelper> = MutableLiveData()

    var bottomLoadingObservable: PublishSubject<Boolean> = PublishSubject.create<Boolean>()

    var progressBottom: ObservableField<Boolean> = ObservableField()

    protected fun showProgress() {
        uiLiveData.value = UiHelper.UiHelperBuilder()
            .showProgress(true)
            .build()
    }

    protected fun showProgress(message: String) {
        var msg = message
        if (message.isEmpty()) {
            msg = CHApplication.applicationContext().getString(R.string.please_wait)
        }
        uiLiveData.value = UiHelper.UiHelperBuilder()
            .showProgress(true)
            .showMessage(msg)
            .build()
    }

    protected fun hideProgress() {
        uiLiveData.value = UiHelper.UiHelperBuilder().showProgress(false).build()
    }

    protected fun setErrorCode(error: NetworkError) {
        val helper = UiHelper.UiHelperBuilder().setError(error).build()
        uiLiveData.value = helper
    }
}