package com.android.cardinalhealthtask.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.cardinalhealthtask.R
import com.android.cardinalhealthtask.common.BaseAdapter
import com.android.cardinalhealthtask.common.BaseHandler
import com.android.cardinalhealthtask.databinding.FragmentMainBinding
import com.android.cardinalhealthtask.model.GalleryFeedItem
import com.android.cardinalhealthtask.view.base.BaseFragment
import com.android.cardinalhealthtask.view.home.viewmodel.HomeViewModel
import javax.inject.Inject
import android.graphics.Rect
import android.util.Log
import com.android.cardinalhealthtask.data.event.CHEvent
import com.android.cardinalhealthtask.data.event.EventConstants
import com.android.cardinalhealthtask.utils.Logger
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * MainFragment encloses gallery view
 */
class MainFragment : BaseFragment(), BaseHandler<GalleryFeedItem> {

    lateinit var binding: FragmentMainBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var adapter: BaseAdapter<GalleryFeedItem>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMainBinding>(
            inflater,
            R.layout.fragment_main,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        retainInstance = true
        initView()
        progressLytVisibility(true)
    }

    private fun getGalleryFeedData() {
        getViewModel()?.fetchGalleryFeedData("")
    }

    /**
     * Init recycler view
     */
    private fun initView() {
        binding.recyclerGalleryList.layoutManager =
            GridLayoutManager(activity?.baseContext, 2)
        val spacing: Int = resources.getDimensionPixelSize(R.dimen.recycler_spacing) / 2

        binding.recyclerGalleryList.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                outRect.set(spacing, spacing, spacing, spacing)
            }
        })
        binding.swipeRefreshLayout.setOnRefreshListener {
            getGalleryFeedData()
        }
        RxTextView.textChangeEvents(binding.editText)
            .skipInitialValue()
            .debounce(500, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(myObserver)
    }

    private var myObserver: io.reactivex.Observer<TextViewTextChangeEvent> =
        object : Observer<TextViewTextChangeEvent>,
            io.reactivex.Observer<TextViewTextChangeEvent> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: TextViewTextChangeEvent) {
                Log.d(TAG, "Search query: " + t.text())
                if (!t.text().isNullOrEmpty()) {
                    getViewModel()?.fetchGalleryFeedData("" + t.text())
                } else {
                    getViewModel()?.fetchGalleryFeedData("")
                }
            }

            override fun onError(e: Throwable) {
            }

            override fun onChanged(t: TextViewTextChangeEvent?) {
            }
        }

    /**
     * get view model
     */
    override fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, viewModelFactory).get(HomeViewModel::class.java)
    }

    /**
     * observe live data from view model
     */
    override fun startObservingLiveData() {
        super.startObservingLiveData()

        getGalleryFeedData()

        // Gallery feed observer
        getViewModel()?.getGalleryFeedData()?.observe(this, Observer {
            hideKeyboard()

            binding.swipeRefreshLayout?.apply {
                isRefreshing = false
            }
            progressLytVisibility(false)
            noItemLytVisibility(false)
            it.let { response ->
                renderListData(response)
            }
        })
    }

    /**
     * Progress bar visibility
     */
    private fun progressLytVisibility(isShow: Boolean) {
        if (isShow) {
            binding.lytProgress.visibility = View.VISIBLE
        } else {
            binding.lytProgress.visibility = View.GONE
        }
    }

    /**
     * Progress bar visibility
     */
    private fun noItemLytVisibility(isShow: Boolean) {
        if (isShow) {
            binding.lytNoItem.visibility = View.VISIBLE
        } else {
            binding.lytNoItem.visibility = View.GONE
        }
    }

    /**
     * Render gallery list
     */
    private fun renderListData(galleryList: ArrayList<GalleryFeedItem>) {
        galleryList?.run {
            if (galleryList.size > 0) {
                adapter = BaseAdapter(R.layout.item_gallery_list, this@MainFragment)
                adapter?.updateList(galleryList)
                binding.recyclerGalleryList.adapter = adapter
            } else {
                noItemLytVisibility(true)
            }
        }
    }

    override fun handleEvents(event: Any) {
        super.handleEvents(event)
        if (event is CHEvent) {
            when (event.event) {
                EventConstants.SORT_BY_TAKEN_DATE -> {
                    Logger.d("Date Taken")
                    getViewModel()?.sortByTakenData()
                }
                EventConstants.SORT_BY_PUBLISHED_DATE -> {
                    Logger.d("Date Published")
                    getViewModel().sortByPublishedDate()
                }
                EventConstants.SORT_BY_NONE -> {
                    Logger.d("None")
                    getViewModel()?.sortByNone()
                }
            }
        }
    }

    /**
     * List item on click handling
     */
    override fun onClick(view: View?, data: GalleryFeedItem?) {
    }
}
