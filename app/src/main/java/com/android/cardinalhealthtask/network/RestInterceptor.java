package com.android.cardinalhealthtask.network;


import com.android.cardinalhealthtask.app.CHApplication;
import com.android.cardinalhealthtask.utils.Logger;
import com.android.cardinalhealthtask.utils.UtilsHelper;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Responsible for modifying actual request and adding headers requested
 */
public class RestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();

        Request modifiedRequest = originalRequest.newBuilder()
                .method(originalRequest.method(), originalRequest.body())
                .header(RestConstants.KEY_ACCEPT_ENCODING, RestConstants.VALUE_IDENTITY)
                .build();

        RequestBody rb = originalRequest.body();
        Buffer buffer = new Buffer();
        if (rb != null)
            rb.writeTo(buffer);

        /*Logging modified and actual request which will hit server*/
        Logger.d(getClass().getName(), RestConstants.NETWORK_REQUEST_PAYLOAD + modifiedRequest.toString());
        Logger.d(getClass().getName(), RestConstants.VALUE_PAYLOAD + buffer.readUtf8() + "\n ::::Headers" + modifiedRequest.headers().toString());

        Response response = chain.proceed(modifiedRequest);

        final String responseString = new String(response.body().bytes());
        Logger.d(getClass().getName(), RestConstants.NETWORK_RESPONSE_PAYLOAD + responseString);

        return response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), responseString))
                .build();

    }
}
