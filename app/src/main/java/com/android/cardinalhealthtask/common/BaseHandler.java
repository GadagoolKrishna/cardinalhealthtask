package com.android.cardinalhealthtask.common;

import android.view.View;

/**
 * A BaseHandler interface is responsible for handling on list click event in {@link BaseAdapter} generic class
 *
 * @param <T> Generic model
 */
public interface BaseHandler<T> {
    void onClick(View view, T data);
}
