package com.android.cardinalhealthtask.utils

import com.android.cardinalhealthtask.BuildConfig

/**
 * CHConstants class holds all the constants correspond to app
 */
class CHConstants {
    companion object {
        const val TITLE_HOME = "Cardinal Health"
        const val TAG = "CH LOGGER"
        const val BASE_URL = BuildConfig.SERVER_BASE_URL
        const val NETWORK_RESPONSE_PAYLOAD = "NETWORK RESPONSE :-"
    }
}