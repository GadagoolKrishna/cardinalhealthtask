package com.android.cardinalhealthtask.utils;

import android.util.Log;

import com.android.cardinalhealthtask.BuildConfig;

/**
 * Logger class handled different form of logging operations in this app
 */
public class Logger {

    private static void writeLogD(String tag, String value) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, value);
        }
    }

    private static void writeLogE(String tag, Exception value) {
        if (BuildConfig.DEBUG) {
            value.printStackTrace();
        }
    }

    public static void d(String value) {
        writeLogD(CHConstants.TAG, value);
    }

    public static void d(String tag, String value) {
        writeLogD(CHConstants.TAG + tag, "" + value);
    }


    public static void e(Exception value) {
        writeLogE(CHConstants.TAG, value);
    }

    public static void e(String tag, Exception value) {
        writeLogE(CHConstants.TAG + tag, value);
    }

}
