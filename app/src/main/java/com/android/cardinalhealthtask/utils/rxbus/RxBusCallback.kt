package com.android.cardinalhealthtask.utils.rxbus

interface RxBusCallback {
    fun onEventTrigger(event: Any)
}