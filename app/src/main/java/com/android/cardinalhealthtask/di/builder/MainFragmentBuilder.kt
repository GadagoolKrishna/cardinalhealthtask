package com.android.cardinalhealthtask.di.builder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.cardinalhealthtask.di.factory.CHViewModelFactory
import com.android.cardinalhealthtask.di.key.ViewModelKey
import com.android.cardinalhealthtask.view.home.MainFragment
import com.android.cardinalhealthtask.view.home.viewmodel.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class MainFragmentBuilder {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: CHViewModelFactory): ViewModelProvider.Factory

    @ContributesAndroidInjector
    internal abstract fun mainFragment(): MainFragment

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel
}