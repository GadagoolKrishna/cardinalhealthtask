package com.android.cardinalhealthtask.data.event

import android.os.Bundle

class ActivityEvent {
    var activity: Int? = 0
    var data: String? = null
    var bundle: Bundle? = null
}