package com.android.cardinalhealthtask.data.event

import android.os.Bundle

class CHEvent {
    var event: Int? = 0
    var eventDesc: String? = null
    var bundle: Bundle? = null
}